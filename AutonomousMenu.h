/*
This is a middleman program that sits between the MenuSystem and the Autonomous program.
*/

#include "MenuSystem.h"

ubyte get_PauseTime()
//Purpose: This function lets the user select and return a wait time for the beginning of their program.
//Contract: NULL ==> Number in milliseconds
{
	ubyte nPauseTime = 0;
	eraseDisplay();
	while(nNxtButtonPressed!= 3)
  {
    nxtDisplayCenteredTextLine(1, "%d Seconds", nPauseTime);
    if(nNxtButtonPressed == 2 && nPauseTime != 0)
    {
    	PlayTone(500, 20);
      nPauseTime--;
      wait10Msec(40);
    }
    if(nNxtButtonPressed == 1)
    {
    	PlayTone(500, 20);
      nPauseTime++;
      wait10Msec(40);
    }
    wait1Msec(40);
  }
  PlayTone(800, 20);
  nxtDisplayCenteredBigTextLine(3, "! LOCK !");
  wait10Msec(100);
  eraseDisplay();
  return nPauseTime;
}
