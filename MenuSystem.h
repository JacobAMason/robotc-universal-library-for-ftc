/*
This is a menu system designed to receive various input and display it in a selection
menu format.
It can be used in many different ways, autonomous program selection being one of them.
*/

//If you need to build a huge menu, you can change this.
//If you do change it, realize that you'll have to change the variable types (lots of bytes and ubytes)
#define MAX_MENU_SIZE 32
#define MAX_ARRAY_INDEX (MAX_MENU_SIZE -1)

typedef struct
//Anything that needs to be added to the menu must be of this type
{
	string sDisplayName;

	//nValue will usually be the number of the program to run, which is why it's an unsigned byte.
	ubyte nValue;
} tMenuItem;

typedef struct
//This is a singleton: There should only be one of these.
{
	tMenuItem item[MAX_MENU_SIZE];
	ubyte nPosition; //Fixes the array-passing bug, too!
	byte nVacantPos; // This will be -1 if all positions are full.
} tMenuArray;

tMenuArray aMenuArray;

void InitializeMenu()
{
	for(ubyte i = 0; i < MAX_MENU_SIZE; i++)
	{
		aMenuArray.item[i].sDisplayName = "";
		aMenuArray.item[i].nValue = 0;
	}
	aMenuArray.nPosition = 0;
	aMenuArray.nVacantPos = 0;
}


bool add_MenuItem(string sText, int nNum)
//will return false if menu is full. Otherwise returns true.
{
	if(aMenuArray.nVacantPos == -1)
	//error case
	{
		return false;
	}else{
	//success case
		aMenuArray.item[aMenuArray.nVacantPos].sDisplayName = sText;
		aMenuArray.item[aMenuArray.nVacantPos].nValue = nNum;

		if(aMenuArray.nVacantPos == MAX_ARRAY_INDEX)
		//If the Item just filled was the last item, set nVacantPos to -1
		{
			aMenuArray.nVacantPos = -1;
			writeDebugStreamLine("nVacantPos = -1");
		}else{
		//otherwise, just increment it.
			aMenuArray.nVacantPos++;
			writeDebugStreamLine("nVacantPos Incremented: %d", aMenuArray.nVacantPos);
		}

		return true;
	}
}


void forward_menu()
//goto next menu item
{
	if(aMenuArray.nPosition == MAX_ARRAY_INDEX)
	//must check to see if we are at the end of the array or else the program will crash
	{
		aMenuArray.nPosition = 0;
	}else	if(aMenuArray.nPosition +1 == aMenuArray.nVacantPos)
	//if the next menu item is not in use, "roll-over" to the 0th position.
	{
		aMenuArray.nPosition = 0;
	}else{
		aMenuArray.nPosition++;
	}
}

void backward_menu()
//goto previous menu item
{
	if(aMenuArray.nPosition == 0)
	//check to see if the aMenuArray is at the beginning
	{
		aMenuArray.nPosition = aMenuArray.nVacantPos -1;
	}else{
	//Don't have to check to see if it is in use because previous indices always will be.
		aMenuArray.nPosition--;
	}
}


void selection_loop(tMenuItem &menuSelection)
//This is the main display loop that shows the current item on the screen
{
	eraseDisplay();
	while(nNxtButtonPressed != 3)
  {
  	nxtDisplayCenteredTextLine(1, "%s", aMenuArray.item[aMenuArray.nPosition].sDisplayName);
  	// The below screen displays are priceless when it comes to debugging this.
    //nxtDisplayTextLine(3, "%d", aMenuArray.nPosition);
    //nxtDisplayTextLine(4, "%d", aMenuArray.nVacantPos);
    if(nNxtButtonPressed == 2)
    {
    	PlayTone(500, 20);
      backward_menu();
      wait1Msec(400);
    }
    if(nNxtButtonPressed == 1)
    {
    	PlayTone(500, 20);
      forward_menu();
      wait1Msec(400);
    }
    wait1Msec(40);
  }
  PlayTone(800, 20);
  nxtDisplayCenteredBigTextLine(3, "! LOCK !");

  menuSelection.sDisplayName = aMenuArray.item[aMenuArray.nPosition].sDisplayName;
  menuSelection.nValue = aMenuArray.item[aMenuArray.nPosition].nValue;
  wait10Msec(100);
}
