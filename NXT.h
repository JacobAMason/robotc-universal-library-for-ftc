#pragma systemFile
//#pragma autoStartTasks

//float INCH_CONVERSION;
//float DEGREE_CONVERSION;

// Used in linear and angular motion
#define WHEEL_DIAMETER (2.25) //in whatever unit (Inches, cm) as long as it matches the unit you input
#define GEAR_RATIO (1/1) // Driver / driven [https://www.youtube.com/watch?v=D_i3PJIYtuY]
#define ENC_COUNTS_PER_REVOLUTION (360 / GEAR_RATIO) // Do not change
#define INCH_CONVERSION (ENC_COUNTS_PER_REVOLUTION/(PI * WHEEL_DIAMETER)) // Do not change

// Used only in angular motion
#define WHEEL_BASE (4.5) //width of the wheel base in the same unit used above.
#define DEGREE_CONVERSION ((WHEEL_BASE*ENC_COUNTS_PER_REVOLUTION)/(360*WHEEL_DIAMETER)) // Do not change
//The actual equation is WHEEL_BASE*PI*ENC_COUNTS_PER_REVOLUTION / 360dg*WHEEL_DIAMETER*PI,
//but the PIs cancel out, so this equation is dramatically simplified.

// Used in gyroscopic turns
#define GYROSCOPE_OFFSET (0)

////////////////////////////////////////////////////////////////////////
//
//	                    Gyroscope Sensor Name
//
////////////////////////////////////////////////////////////////////////
// This allows the Gyroscope sensor name to be passed into this program and turn it
// into a global variable.
// syntax: get_gyroscope_sensor(Gyro sensor name);
// Put this before the waitForStart(); function.

tSensors Gyroscope;
bool IS_GYRO_SENSOR = false;

void get_gyroscope_sensor(tSensors gyroTemp)
{
	Gyroscope = gyroTemp;
	IS_GYRO_SENSOR = true;
}

////////////////////////////////////////////////////////////////////////
//
//                   		Halting and Debugging
//
////////////////////////////////////////////////////////////////////////

//optional debugging function. You can drop a "debugstop();" at any point in the code
//to have it pause processing until the orange button on the NXT is pressed
void debug_stop()
{
	while(nNxtButtonPressed!=3)
	{}
	return;
}

// function used to stop the drive train for the next movement,
// pausing for one second.
void halt_drive()
{
	motor[motorB]= 0; motor[motorC]= 0;
	wait1Msec(800); //Wait for the robot to actually stop moving
	return;
}

////////////////////////////////////////////////////////////////////////
//
//												Motion Controls
//
////////////////////////////////////////////////////////////////////////

//
// pMotor Core System
//

long GYRO_TIME = 0; //Used to Integrate the Gyroscope
long GYRO_ANGULAR_DISPLACEMENT = 0;

bool targeting_system(ubyte TrackingMethod, int Target)
/*
This system is responsible for returning a boolean for whether a movement has reached its target.
*/
{
	switch(TrackingMethod)
	{
	case 1://Encoder based movement
		return ((abs(nMotorEncoder[motorB]) < Target) || (abs(nMotorEncoder[motorC]) < Target));

	case 2://Gyroscopic based movement	Works in theory
		long angularVelocity = SensorValue(Gyroscope) - GYROSCOPE_OFFSET;
		float Gyro_ChangeInTime = time1[T2] - GYRO_TIME;
		GYRO_ANGULAR_DISPLACEMENT += angularVelocity * Gyro_ChangeInTime;
		return abs(GYRO_ANGULAR_DISPLACEMENT) < Target; //Gyroscope has yet to reach target should return True
	}
	return false; //This return should never execute, but the compiler looks for it regardless.
}

bool pMotor(int Target, byte LeftSpeed, byte RightSpeed, ubyte TrackingMethod = 1)
{
	bool stopMotors = false;
	int changeInCounts = 0;
	int previousCounts = -100; //This is an offset for the first check since the robot is
														 //just still accelerating.
	int threshhold = ((abs(LeftSpeed) + abs(RightSpeed))/2) * 5; //Doesn't matter which speed we use, but we'll average them just in case we want to direct access this function
	long previousTime = time100[T2];														 // abs() of each speed is taken because they can sometimes be in opposite directions
	GYRO_TIME = time1[T2];
	GYRO_ANGULAR_DISPLACEMENT = 0;

	nMotorEncoder[motorB]= 0; nMotorEncoder[motorC]= 0;

	while( targeting_system(TrackingMethod, Target) && (stopMotors == false))
	{
		motor[motorB]= LeftSpeed; motor[motorC]= RightSpeed;

		if(time100[T2] > 6+previousTime)
		{
												//get the average distance traveled on both wheels
			changeInCounts = ((abs(nMotorEncoder[motorB]) + abs(nMotorEncoder[motorC])) / 2) - previousCounts;
			previousCounts = (abs(nMotorEncoder[motorB]) + abs(nMotorEncoder[motorC])) / 2;

			if (changeInCounts < threshhold)
			{
				stopMotors = true;
				PlayTone(440, 30);
			}
			writeDebugStreamLine("nMotorEncoder[motorB] = %d  nMotorEncoder[motorC] = %d", nMotorEncoder[motorB], nMotorEncoder[motorC]);
			writeDebugStreamLine("changeInCounts = %d \t threshhold = %d", changeInCounts, threshhold);
			previousTime = time100[T2];
		}
	}
	halt_drive();
	return (!stopMotors); // This is the success value. If stopMotors is true, then it didn't succeed and vice versa.
}

//
//  Linear motion
//


bool linear(int inchTarget, byte speed, bool deltaCorrection = false)
{
	byte LeftSpeed = speed * sgn(inchTarget) * 0.9;
	byte RightSpeed = LeftSpeed;
	int encoderTarget = abs(inchTarget * INCH_CONVERSION); //convert from inches to encoder counts and take the absolute value so we just have a distance
	writeDebugStreamLine("\nlinear movement: %d inches  %d Encoder Target  %d LeftSpeed  %d RightSpeed", inchTarget, encoderTarget, LeftSpeed, RightSpeed);

	bool movementSuccess = pMotor(encoderTarget, LeftSpeed, RightSpeed);

	if(movementSuccess && deltaCorrection)
	{
		int drift = (((abs(nMotorEncoder[motorB]) + abs(nMotorEncoder[motorC])) / 2) - abs(encoderTarget)); //initial - final returns a negative number
		int driftLeft = LeftSpeed/-5;
		int driftRight = RightSpeed/-5;
		movementSuccess = (movementSuccess && pMotor(drift, driftLeft, driftRight) );
		writeDebugStreamLine("correction: Encoder counts reversed: %d  driftLeft: %d  driftRight %d", drift, driftLeft, driftRight);
	}

	if(movementSuccess)
	{
		writeDebugStreamLine("movementSuccess: SUCCESS");
	}else{
		writeDebugStreamLine("movementSuccess: FAILURE");
	}

	return movementSuccess;
}


//
//	Angular motion
//

bool angular(int degrees, byte speed, bool deltaCorrection = false)
/*
angular checks if there is a gyro sensor in use. If there is, it defaults to using it to calculate movement. If not, it calculates movements using encoders.
positive degrees turn the robot Left. Negative degrees turn it Right.
*/
{
	byte RightSpeed = speed * sgn(degrees);
	byte LeftSpeed = -RightSpeed;
	byte Target;
	bool movementSuccess;

	if(IS_GYRO_SENSOR)
	{
		Target = degrees;
		writeDebugStreamLine("\nangular movement *GYRO*: %d degrees  %d LeftSpeed  %d RightSpeed", degrees, LeftSpeed, RightSpeed);
		movementSuccess = pMotor(Target, LeftSpeed, RightSpeed, 2);
  }else{
		Target = abs(degrees * DEGREE_CONVERSION); //convert from degrees to encoder counts
		writeDebugStreamLine("\nangular movement: %d degrees  %d Encoder Target  %d LeftSpeed  %d RightSpeed", degrees, Target, LeftSpeed, RightSpeed);
		movementSuccess = pMotor(Target, LeftSpeed, RightSpeed);
	}

	if(movementSuccess && deltaCorrection)
	{
		byte drift = (((abs(nMotorEncoder[motorB]) + abs(nMotorEncoder[motorC])) / 2) - abs(degrees * DEGREE_CONVERSION)); //initial - final returns a negative number
		byte driftLeft = LeftSpeed/-5;
		byte driftRight = RightSpeed/-5;
		movementSuccess = (movementSuccess && pMotor(1, drift, driftLeft, driftRight) );
		writeDebugStreamLine("correction: Encoder counts reversed: %d  driftLeft: %d  driftRight %d", drift, driftLeft, driftRight);
	}

	if(movementSuccess) //Just a debug write
	{
		writeDebugStreamLine("movementSuccess: SUCCESS");
	}else{
		writeDebugStreamLine("movementSuccess: FAILURE");
	}

	return movementSuccess;
}


////////////////////////////////////////////////////////////////////////////////
//
//                          Calibration File Reading  Not ready to implement
//
////////////////////////////////////////////////////////////////////////////////
//
//
//task read_calibration_file()
//{
//	TFileHandle myFileHandle;          // create a file handle variable 'myFileHandle'
//	TFileIOResult IOResult;            // create an IO result variable 'IOResult'
//	string myFileName = "Calibration.txt";   // create and initialize a string variable 'myFileName'
//	int myFileSize;                    // create and initialize an integer variable 'myFileSize'

//	OpenRead(myFileHandle, IOResult, myFileName, myFileSize);
//	ReadFloat(myFileHandle, IOResult, INCH_CONVERSION);
//	ReadFloat(myFileHandle, IOResult, DEGREE_CONVERSION);
//	ReadFloat(myFileHandle, IOResult, GYROSCOPE_OFFSET);
//}
