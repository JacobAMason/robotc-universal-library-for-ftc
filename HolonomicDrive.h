
#define JOYSTICK_DEADBAND  			15
// Returns an angle that is within -179 to 180 degrees
float FixDirection (float Angle)
{ // FixDirection
	float NewAngle;

	if (Angle > 180.0)
		NewAngle = Angle - 360.0;
	else if (Angle < -180.0)
		NewAngle = Angle + 360.0;
	else
		NewAngle = Angle;
	return NewAngle;
} // end FixDirection

float getMagnitude()
{
	float TempMagnitude;

	TempMagnitude = sqrt((int)pow(joystick.joy1_y1* 100.0/128.0, 2)+(int)pow(joystick.joy1_x1* 100.0/128.0, 2));
	if (TempMagnitude > 100.0) TempMagnitude = 100.0;
	//use logarithmic scaling to provide more sensitivity at slow speeds
	TempMagnitude = pow(TempMagnitude, 2)/pow(100, 2) * 100.0;
	return TempMagnitude;
}

float getDirection()
{
	float D = radiansToDegrees(atan2(joystick.joy1_y1* 100.0/128.0, -joystick.joy1_x1* 100.0/128.0));
	return FixDirection(D-45);
}

float getSpin()
{
	float S = joystick.joy1_x2*(100.0/128.0);
	return S;
}

// magnitude is 0 to 100, direction -179 to 180, spin is -100 to 100
void HolonomicDrive (float Magnitude, float Direction, float Spin)
{ // HolonomicDrive
	float AdjustedDirection;
	float cosD;
	float sinD;
	float SpeedLeftFrontRightBack;
	float SpeedRightFrontLeftBack;
	float SpinLeft;
	float SpinRight;
  float SpeedRightFront;
	float SpeedLeftFront;
	float SpeedRightBack;
	float SpeedLeftBack;


	if (abs(Spin) > JOYSTICK_DEADBAND)
	{ //spin robot
		// calculate the amount of motor adjustment to spin robot
	   	SpinLeft = -Spin*Spin/abs(Spin);
	   	SpinRight = Spin*Spin/abs(Spin);
	} // end spin robot
	else
	{//don't spin robot
	   	SpinLeft = 0.0;
           	SpinRight= 0.0;
	}//end don't spin robot

	if (Magnitude > JOYSTICK_DEADBAND)
	{ // Move Robot
		AdjustedDirection = degreesToRadians(Direction);
		cosD = cos(AdjustedDirection);
		sinD = sin(AdjustedDirection);

		// Calculate motor speeds based on magnitude and direction
		SpeedLeftFrontRightBack = (sinD * Magnitude * sqrt(2.0));
		SpeedRightFrontLeftBack = (cosD * Magnitude  * sqrt(2.0));

		//Scaling the motor speeds if over 100
		if (abs(SpeedLeftFrontRightBack) > 100.0)
		{
			SpeedLeftFrontRightBack = SpeedLeftFrontRightBack / abs(SpeedLeftFrontRightBack) * 100.0;
			SpeedRightFrontLeftBack = SpeedRightFrontLeftBack / abs(SpeedLeftFrontRightBack) * 100.0;
		}
		if (abs(SpeedRightFrontLeftBack) > 100.0)
		{
			SpeedRightFrontLeftBack = SpeedRightFrontLeftBack / abs(SpeedRightFrontLeftBack) * 100.0;
			SpeedLeftFrontRightBack = SpeedLeftFrontRightBack / abs(SpeedRightFrontLeftBack) * 100.0;
		}
	} // end move robot
	else
	{ // Stop Robot
		SpeedLeftFrontRightBack = 0.0;
		SpeedRightFrontLeftBack = 0.0;
	} // end Stop Robot


	SpeedLeftFront = SpeedLeftFrontRightBack+SpinLeft;
	SpeedRightFront = SpeedRightFrontLeftBack+SpinRight;
	SpeedLeftBack = SpeedRightFrontLeftBack+SpinLeft;
	SpeedRightBack = SpeedLeftFrontRightBack+SpinRight;

	if(abs(SpeedLeftFront) > 100)
        	SpeedLeftFront = 100*SpeedLeftFront/abs(SpeedLeftFront);
	if(abs(SpeedRightFront) > 100)
        	SpeedRightFront = 100*SpeedRightFront/abs(SpeedRightFront);
	if(abs(SpeedLeftBack) > 100)
        	SpeedLeftBack = 100*SpeedLeftBack/abs(SpeedLeftBack);
	if(abs(SpeedRightBack) > 100)
        	SpeedRightBack = 100*SpeedRightBack/abs(SpeedRightBack);

	//moving motors
	motor[LeftFront] = LeftFront;
	motor[RightFront] = RightFront;
	motor[LeftBack] = LeftBack;
	motor[RightBack] = RightBack;
} // end HolonomicDrive
