# CalibrationDataParser.py
# Jacob Mason  12/18/13
# This python script is designed to parse the Data.txt file produced by the robot.
#

from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
import numpy as np
import math
import pickle
import inspect



aDataPointObjs = []
Robot = None

aTextList = ["Run number",
             "Motor speed",
             "Encoder value after first segment",
             "Encoder value after second segment",
             "Time taken to travel first segment",
             "Time taken to travel both segments",
             "Velocity during first segment",
             "Velocity during second segment",
             "Acceleration over first segment",
             "Acceleration over second segment",
             "Average acceleration",
             "Drift",
             "Slip"]


class RobotConstruction():
  """
  Singleton that contains values that pertain to the entire set of data.
  Battery level is the only value that will be slightly different between tests.
  Contains:
    nTrials
    fWheelDiameter
    fGearRatio
    fWheelBase
    nAvgBatteryLevel
  """
  def __init__(self, asRobotConstruction):
    self.nTrials = int(asRobotConstruction[0])
    self.fWheelDiameter = float(asRobotConstruction[1])
    self.fGearRatio = float(asRobotConstruction[2])
    self.fWheelBase = float(asRobotConstruction[3])
    self.nAvgBatteryLevel = int(asRobotConstruction[4])

def Encoder_To_Inches(nEncoderCounts):
  """
  Purpose: Uses the Robot singleton data to convert Encoder Counts to Inches.
  Contract: Number ==> Number
  """
  return nEncoderCounts / ((360/Robot.fGearRatio)/(math.pi*Robot.fWheelDiameter))

def Inches_To_Encoder(nInches):
  """
  Purpose: Uses the Robot singleton data to convert Inches to Encoder Counts.
  Contract: Number ==> Number
  """
  return round(nInches * ((360/Robot.fGearRatio)/(math.pi*Robot.fWheelDiameter)))




class DataPoint:
  """
  Purpose: This class contains all the information collected from a single iteration of the Calibration tool.
  How To Use: First, avoid referencing the array nALL outside this class. Instead, use aALL.
              To progress with this project, I am creating function that manipulate the obvious data
              contained in all the objects of this class until I find a series of operations
              that will yield a mathematical function that represents the relationship between
              a robot's target and it's predicted drift.
  """
  def __init__(self, point):    
    self.nALL = []
    
    for nItem in point:
      self.nALL.append(int(nItem))
    
    
    # Used to derive other values, but never used directly. Don't forget they are still here:
    self.nOriginalRunNumber = self.nALL[0]
    self.nSpeed = self.nALL[1]
    self.nFirstLeftPos = self.nALL[2]
    self.nFirstRightPos = self.nALL[3]
    self.nFirstTime = self.nALL[4] 
    self.nSecondLeftPos = self.nALL[5]
    self.nSecondRightPos = self.nALL[6]
    self.nSecondTime = self.nALL[7]
    self.nLeftDrift = self.nALL[8]
    self.nRightDrift = self.nALL[9]
    
  def get_RunNumber(self):
    return aDataPointObjs.index(self) +1
  
  def get_Speed(self):
    return self.nSpeed
  
  def get_FirstPosition(self):
    return (self.nFirstLeftPos + self.nFirstRightPos)/2
  
  def get_FirstTime(self):
    return self.nFirstTime
  
  def get_SecondPosition(self):
    return (self.nSecondLeftPos + self.nSecondRightPos)/2  
  
  def get_SecondTime(self):
    return self.nSecondTime
  
  def get_FirstVelocity(self):
    return (self.get_FirstPosition()/self.get_FirstTime())
  
  def get_SecondVelocity(self):
    return (self.get_SecondPosition() - self.get_FirstPosition()) / (self.get_SecondTime() - self.get_FirstTime())
  
  def get_FirstAcceleration(self):
    return (self.get_FirstVelocity() / self.get_FirstTime())
  
  def get_SecondAcceleration(self):
    return (self.get_SecondVelocity() - self.get_FirstVelocity()) / (self.get_SecondTime() - self.get_FirstTime())
  
  def get_AverageAcceleration(self):
    return (self.get_FirstAcceleration() + self.get_SecondAcceleration())/2.0
  
  def get_Drift(self):
    """
    Drift is the measurement in encoder counts from when the line was seen to
    when the robot stopped.
    """
    return (self.nLeftDrift + self.nRightDrift)/2  
  
  def get_Slip(self):
    """
    Slip is the difference in encoder counts between the target encoder number
    and the encoder value when the line was seen.
    """
    return (self.get_SecondPosition() - Inches_To_Encoder(24))
  
  def build_array(self):
    self.aALL = [self.get_RunNumber(), self.get_Speed(), self.get_FirstPosition(),
                 self.get_SecondPosition(), self.get_FirstTime(), self.get_SecondTime(),
                 self.get_FirstVelocity(), self.get_SecondVelocity(),
                 self.get_FirstAcceleration(), self.get_SecondAcceleration(),
                 self.get_AverageAcceleration(), self.get_Drift(), self.get_Slip()]   
  
    
  def get_Direction(self):
    """
    Purpose: This returns the direction the robot was traveling.
             1 is forward, -1 is backward.
    """
    return ((2*(self.nOriginalRunNumber%2)-1))
  
  def get_value(self, nValueToRetrieve):
    """
    Used by the average() function.
    """
    nIndex = nValueToRetrieve
    return self.aALL[nIndex]
    
  def __str__(self):
    sReturnString = ""
    for i in range(len(self.aALL)):
      sReturnString += (aTextList[i] + ": " + str(self.aALL[i]) + "\n")
    return sReturnString    


def print_everything():
  """
  Purpose: Gives a full read-out of all the data in the system.
  Contract: None ==> None, prints literally everything
  """
  for obj in aDataPointObjs:
    print(obj)
    
def tests_run():
  """
  Purpose: Tells the user how many tests are in the system.
  Contract: None ==> Number
  """
  return len(aDataPointObjs)


def get_with_Direction(nDirection):
  """
  Purpse: This function returns a list of objects with the Direction given.
  Contract: Number ==> List of Objects
  Usage: 1 = Forward   -1 = Backward
  """
  aObjsToReturn = []
  for obj in aDataPointObjs:
    if (obj.get_Direction() == nDirection):
      if(inspect.stack()[1][3] == "<module>"): print(obj)
      aObjsToReturn.append(obj)
  return aObjsToReturn

def get_with_Speed(nSpeed):
  """
  Contract: Number ==> List of objcts
  Verbose Mode: This function prints out all data of a run with the speed given.
  """
  aObjsToReturn = []
  for obj in aDataPointObjs:
    if (obj.get_Speed() == nSpeed):
      if(inspect.stack()[1][3] == "<module>"): print(obj)
      aObjsToReturn.append(obj)
  return aObjsToReturn

def get_with_RunNumber(*argsRunValues):
  """
  Purpose:  This function prints out all data associated with a particular run
            number or numbers passed in the form of a list or separated commas.
  Contract: List of Numbers ==> List of objects
            Numbers seperated by commas ==> List of objects
            Number ==> object
  """
  aObjsToReturn = []
  for aRunValues in argsRunValues:
    try:
      iterator = iter(aRunValues)
    except TypeError:
      for obj in aDataPointObjs:
        if(obj.get_RunNumber() == aRunValues):
          if(inspect.stack()[1][3] == "<module>"): print(obj)
          aObjsToReturn = obj
          break
    else:
      for obj in aDataPointObjs:
        if(obj.get_RunNumber() in aRunValues):
          if(inspect.stack()[1][3] == "<module>"): print(obj)
          aObjsToReturn.append(obj)
  #END-for loop
  return aObjsToReturn
  
def average(objects = aDataPointObjs, nThingToAverage = -1):
  """
  Purpose: This is a user-friendly averaging function and shouldn't be used in
           any other functions. Instead, use map_values.
  Contract: List of objects (optional: defaults to all objects), averaging index
            (optional) ==> Number
  """
  if(nThingToAverage == -1):
    print("What do you want to average?")
    for i in range(len(aTextList)-1):
      print(i+1, ": ", aTextList[i+1], ".", sep="")
    print()
    nThingToAverage = int(input(": "))
    while(nThingToAverage not in range(1,len(aTextList))):
      nThingToAverage = int(input("Try a number in the list above: "))
  elif(nThingToAverage not in range(1,len(aTextList))):
    print("ERROR: average: did not receive a decent averaging index")
    return
  
  nTotal = 0
  for obj in objects:
    nTotal += obj.get_value(nThingToAverage)
      
  nAverageToReturn = nTotal / len(objects)

  return nAverageToReturn


def predict_drift(obj):
  """
  Purpose: To compare a drift prediction formula result to the actual drift.
  Contract: Object ==> float Number
  """
  
  ########### Begin Drift Prediction Formula ###########
  
  fDriftPrediction = ((obj.get_SecondVelocity() ** 2) / (2 * obj.get_FirstAcceleration()))
  
  ############ End Drift Prediction Formula ############
  if(inspect.stack()[1][3] == "<module>"):
    print()
    print("      Run Number: ", obj.get_RunNumber())
    print("Drift Prediction: ", fDriftPrediction)
    print("    Actual Drift: ", obj.get_Drift())
    print("      Difference: ", abs(fDriftPrediction - obj.get_Drift()))
    print()
  
  return fDriftPrediction

def predict_deceleration(obj):
  """
  Purpose: To predict the deceleration of the robot as it stops itself.
  Contract: Object ==> Float Number
  """
  
  ########### Begin Deceleration Prediction Formula ###########
  
  fDecelerationPrediction = ((obj.get_SecondVelocity() ** 2) / (2 * obj.get_Drift()))
  
  ############ End Deceleration Prediction Formula ############  
  if(inspect.stack()[1][3] == "<module>"):
    print()
    print("    Deceleration Prediction: ", fDecelerationPrediction)
    print("Acceleration over Segment 1: ", obj.get_FirstAcceleration())
    print("                 Difference: ", abs(fDecelerationPrediction - obj.get_FirstAcceleration()))
    print()  
  
  return fDecelerationPrediction

def map_values(Function, aIterable, bAverage = False):
  """
  Purpose: To quickly perform a function on many data points.
  Use "True" at the end to return an average.
  """
  aResultsToReturn = []
  for value in aIterable:
    aResultsToReturn.append(Function(value))
  if(bAverage):
    nTotal = 0
    for value in aResultsToReturn:
      nTotal += value
    nTotal /= len(aResultsToReturn)
    aResultsToReturn = nTotal
    
  return aResultsToReturn


def plot_VD_points():
  global aDataPointObjs
  
  aVelocityForward = map_values(DataPoint.get_SecondVelocity, get_with_Direction(1))
  aVelocityBackward = map_values(DataPoint.get_SecondVelocity, get_with_Direction(-1))
  aDecelerationForward = map_values(predict_deceleration, get_with_Direction(1))
  aDecelerationBackward = map_values(predict_deceleration, get_with_Direction(-1))
  
  aAverageVelocity = []
  aAverageDeceleration = []  
  for i in range(50, 91, 5):
    aAverageVelocity.append(map_values(DataPoint.get_SecondVelocity, get_with_Speed(i), True))
    aAverageDeceleration.append(map_values(predict_deceleration, get_with_Speed(i), True))
    
  fLowerVelocity = min(aAverageVelocity)
  fLowerDeceleration = min(aAverageDeceleration)
  
  plt.plot(aVelocityForward, aDecelerationForward, "go", aVelocityBackward, aDecelerationBackward, "ro",
           aAverageVelocity, aAverageDeceleration, "-")
  plt.xlabel("Velocity")
  plt.ylabel("Deceleration")  
  plt.show()
  
  return





def add_data(sDataFile = "Data.txt"):
  """
  Purpose: Use this function to take all the data from sDataFile and add it to 
           our working set of data.
  """
  global Robot
  global aDataPointObjs
  File = open(sDataFile, "r")
  aDataList = []
  for line in File:
    line = line.strip()
    line = line.split(", ")
    aDataList.append(list(line))
  
  File.close()
  
  if(aDataList.pop() != ['EOF']):
    print("ERROR: Corrupted Data File")
    exit(1)
  
  if(Robot == None):
    Robot = RobotConstruction(aDataList.pop(0))
  else:
    tempRobot = RobotConstruction(aDataList.pop(0))
    if(tempRobot.fWheelDiameter != Robot.fWheelDiameter or tempRobot.fGearRatio != Robot.fGearRatio
       or tempRobot.fWheelBase != Robot.fWheelBase):
      print("ERROR: Robot Construction from these Data sets does not match.")
      exit(1)  
  
  for line in aDataList:
    aDataPointObjs.append(DataPoint(line))
    
  for obj in aDataPointObjs:
    obj.build_array()

    
def load_data():
  global Robot
  global aDataPointObjs
  try:
    File = open("CalibrationData.dat", "rb")
  except IOError:
    print("ERROR: Could not find 'CalibrationData.dat'  Have you imported any data yet?")
    return
  
  tempRobot = pickle.load(File)
  if(Robot != None):
    if(tempRobot.fWheelDiameter != Robot.fWheelDiameter or tempRobot.fGearRatio != Robot.fGearRatio
       or tempRobot.fWheelBase != Robot.fWheelBase):
      print("ERROR: Robot Construction from these Data sets does not match.")
      return
  Robot = tempRobot
  EOF = False
  while not EOF:
    try:
      aDataPointObjs.extend(pickle.load(File))
    except EOFError:
      EOF = True
  File.close()
  
  for obj in aDataPointObjs:
    obj.build_array()  
  
  print("Calibration Data loaded successfully:", len(aDataPointObjs), "Data Points active.")
  sHeader = "Over " + str(len(aDataPointObjs)) + " trials: 24 inches = " + \
            str(Inches_To_Encoder(24)) + " encoder counts."
  print(sHeader)
  print("Measurements are in Encoder Counts, Milliseconds, and derivatives thereof.")   

  
def save_data():
  global Robot
  global aDataPointObjs
  try:
    File = open("CalibrationData.dat", "rb")
    tempRobot = pickle.load(File)
    File.close()
  except IOError:
    File = open("CalibrationData.dat", "wb")
    pickle.dump(Robot, File)
    pickle.dump(aDataPointObjs, File)
    File.close()
    print("SUCCESS: new file created")
  else:
    if(tempRobot.fWheelDiameter != Robot.fWheelDiameter or tempRobot.fGearRatio != Robot.fGearRatio
       or tempRobot.fWheelBase != Robot.fWheelBase):
      print("ERROR: Robot Construction from these Data sets does not match.")
      return
    else:
      File = open("CalibrationData.dat", "wb")
      pickle.dump(Robot, File)
      pickle.dump(aDataPointObjs, File)
      File.close()
      print("SUCCESS: updated")
      

def erase_data():
  global aDataPointObjs
  global Robot
  aDataPointObjs = []
  Robot = None  
      

def help(Function = -1):
  """
  Purpose: Prints the docstring of a given function.
  Contract: Function ==> None, prints docstring
  """
  if(Function != -1):
    print(Function.__doc__.strip().replace("\n  ", "\n"))
  else:
    print(help.__doc__.strip().replace("\n  ", "\n"))
  
    
load_data()
#aDataPointObjs, Robot = add_data_file("Data.txt", Robot)