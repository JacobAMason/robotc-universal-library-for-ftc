int JoystickThreshold = 10;
bool BurnoutProtection = false;
long CurrEncValueLeft = -1;
long CurrEncValueRight = -1;
long LastEncoderValueLeft = -1;
long LastEncoderValueRight = -1;
bool RobotMoving = false;
bool RobotStalled = false;
int LeftMotorPower = 0;
int RightMotorPower = 0;

//move the robot based on the magnitude and direction recieved from the joystick
//
void TankDrive (float Magnitude, float Direction, )
{ // TankDrive

	int MotorRightSpeed;
	int MotorLeftSpeed;

	CurrEncValueLeft = nMotorEncoder(LeftMotor);
	CurrEncValueRight = nMotorEncoder(RightMotor);

        if(abs(Magnitude) < JoystickThreshold)
          Magnitude = 0;
        if(abs(Direction) < JoystickThreshold)
          Direction = 0;

	if(BurnoutProtection)
	{
		// check if robot is commanded to move
		if((abs(Magnitude) > JoystickThreshold) || (abs(Direction) > JoystickThreshold) && !RobotMoving)
		{
			RobotMoving = true;
			ClearTimer(T3);
		}

		if (((abs(Magnitude) < JoystickThreshold) && (abs(Direction) < JoystickThreshold)))
		{
			RobotMoving = false;
			RobotStalled = false;
		}

		if (RobotStalled)
			return;

		if (RobotMoving)
		{
			if(time1[T3] > 500)
			{
				ClearTimer(T3);
				if((CurrEncValueLeft == LastEncoderValueLeft) || (CurrEncValueRight == LastEncoderValueRight))
				{
					Magnitude = 0;
					Direction = 0;
					RobotStalled = true;
				}
				LastEncoderValueRight = CurrEncValueRight;
				LastEncoderValueLeft = CurrEncValueLeft;
			}
		}
	} //end if burnout protection

		LeftMotorPower = Magnitude+Direction;
		RightMotorPower = Magnitude-Direction;

        //
        // Insert code to move motors either here or in main file
        //

} // end TankDrive
