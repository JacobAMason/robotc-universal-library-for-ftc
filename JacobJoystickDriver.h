#pragma systemFile
#pragma autoStartTasks

#include "JoystickDriver.c"

#warning "YOU ARE USING A MODIFIED JOYSTICK DRIVER"

////////////////////////////////////////////////////////////////////////
//
//                     		Button Toggling
//
////////////////////////////////////////////////////////////////////////

// Initialize all the boolean arrays necessary for button toggling
bool joy1BtnBool[12], joy2BtnBool[12], joy1BtnPrevState[12], joy2BtnPrevState[12];

// Transform those variables into arrays with nothing but false boolean values
task initvariables(){
	memset(joy1BtnBool, false, sizeof(joy1BtnBool));
	memset(joy2BtnBool, false, sizeof(joy2BtnBool));
	memset(joy1BtnPrevState, false, sizeof(joy1BtnPrevState));
	memset(joy2BtnPrevState, false, sizeof(joy2BtnPrevState));
}

// We call the function joy#Btn(), passing the value of button to that function, returning a boolean for the button press.
// The index is always the number of the button minus one because there is no zero button but there is a zero index.

short joy1Tog(ubyte btn)
{
	// if the button is being pressed
	if(joy1Btn(btn))
	{
		//if the past value of the array is true, this is the first iteration in which the button is pressed.
		if(joy1BtnPrevState[btn-1])
		{
			//change the array to false so we don't run this loop more than once
			joy1BtnPrevState[btn-1] = false;
			joy1BtnBool[btn-1] = !joy1BtnBool[btn-1];
		}
	}else{
		joy1BtnPrevState[btn-1] = true;
	}
	return joy1BtnBool[btn-1]; // return the state of the toggle
}

short joy2Tog(ubyte btn)
{
	// if the button is being pressed
	if(joy2Btn(btn))
	{
		//if the past value of the array is true, this is the first iteration in which the button is pressed.
		if(joy2BtnPrevState[btn-1])
		{
			//change the array to false so we don't run this loop more than once
			joy2BtnPrevState[btn-1] = false;
			joy2BtnBool[btn-1] = !joy2BtnBool[btn-1];
		}
	}else{
		joy2BtnPrevState[btn-1] = true;
	}
	return joy2BtnBool[btn-1]; //return the state of the toggle.
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
