//
// This file has been modified from the original c file to serve as a header.
//



////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                               Text File Input / Output
//
//                              (actually, just output now)
//
//
// This program demonstrates how to read and write numeric values form a standard Windows/Mac
// text file. The text file could be created on the PC and downloaded/uploaded to the NXT.
//
// ROBOTC on the NXT provides "native" functions for reading and writing "raw" numbers to a file.
// A raw number is the encoded format of the number and is not easily readable or editable on the
// PC without special editors. For example, writing a 'long' variable to a NXT file writes four
// bytes.
//
// This demonstration program provides several sample functons that can be easily copied to a
// user's application program. It uses standard human readable text files.
//
// The functions to write a text file include:
//   1. Open the file
//   2. Write a integer value as text to the file. Values on the same line are separated with commas
//      and blanks.
//   3. Begin a new text line
//   4. Close the file
//
///////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma platform(NXT)     // This program only works on NXT -- generate error for other platforms

///////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                             Functions for writing a text file
//
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool bOpenWriteTextFile(const string &sFileName, int nFileSize);
void closeWriteTextFile();
void writeNewLine();
void writeIntegerNumber(long  nNumber);
void writeFloatNumber(float fNumber);
void from_NXT_TEXT_FILE_IO_WriteText(string sTemp);

///////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                      Implementation of "Write Text File" Functions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////

TFileHandle hFileWriteHandle = NOT_A_HANDLE;

bool bFirstNumberOnLine      = true;
TFileIOResult nIoResult      = ioRsltSuccess;

bool bOpenWriteTextFile(const string &sFileName, int nFileSize)
{
	bFirstNumberOnLine = true;
	Delete(sFileName, nIoResult);
	OpenWrite(hFileWriteHandle, nIoResult, sFileName, nFileSize);
	return nIoResult == ioRsltSuccess;
}


void closeWriteTextFile()
{
	Close(hFileWriteHandle, nIoResult);
	hFileWriteHandle = NOT_A_HANDLE;
	return;
}

void writeNewLine()
{
	//
	// Writes carriage return and new line characters to start a new line
	//
	WriteText(hFileWriteHandle, nIoResult, "\r\n");
	bFirstNumberOnLine = true;
	return;
}



void writeDelimiterBetweenNumbers()
{
	//
	// If required, writes ", " between numbers on same line
	//
	if (bFirstNumberOnLine)
		bFirstNumberOnLine = false;
	else
		WriteText(hFileWriteHandle, nIoResult, ", ");
	return;
}


void writeIntegerNumber(long nNumber)
{
	//
	// Writes an integer to the file
	//
	string sTemp;

	writeDelimiterBetweenNumbers();
	//
	// Modify format code ("%d") if you want to change the format -- say to line up the
	// columns for your application; e.g. "%5d" will make every number five characters.
	//
	StringFormat(sTemp, "%d", (long) nNumber);
	WriteText(hFileWriteHandle, nIoResult, sTemp);
	return;
}

void writeFloatNumber(float fNumber)
{
	//
	// Writes a floating point number to the line
	//
	string sTemp;

	writeDelimiterBetweenNumbers();
	StringFormat(sTemp, "%f", fNumber);
	WriteText(hFileWriteHandle, nIoResult, sTemp);
	return;
}

void from_NXT_TEXT_FILE_IO_WriteText(string sTemp)
{
	WriteText(hFileWriteHandle, nIoResult, sTemp);
	return;
}
