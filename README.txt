This is an open source package designed for implementation on any "standard" FTC robot. If your bot has a four motor drivetrain with at least one encoder per side, this library will provide some getting started material for your team. I encourage you to figure out how this code works and then modify it to suit your needs. I discourage you from entering the judging room claiming this code as your own. I can't really do anything about that, but I'd be disappointed.

If you are unsure how to use Git and Bitbucket, I encourage you to read Atlassian's tutorials on Git. If you just want to download the most updated piece of software, it's probably wisest to at least follow this repository's RSS feed. If you have trouble getting this code downloaded or if you have a bug/question/good idea, don't hesitate to message me.

Best way to use these files:

These are all header files. This .h extension tells RobotC that this program cannot serve as a standalone program, but must be included by another program. While this "suite" or "toolkit" is powerful, it cannot replace your main program.

In order to set up RobotC to work fluently with this package, you'll need to drop all these header files into a folder. Name it something creative and obvious. Then, inside RobotC, select View > Preferences > Detailed Preferences ... > Compiler ...  Click on the tab that says "Include Directories." Next click the "Browse.." button besides the first blank and navigate to the folder you put all these header files in.




	Headers


UniversalLib.h

The UniversalLib header is just a combination of JacobJoystickDriver.h and Tetrix.h. While it may seem like the  thing to include in everything, most of you probably have separate Autonomous and TeleOp programs. It logically follows that UniversalLib is a waste of space and that's right. It is. Don't use it. It's only really there to confuse you. If I see someone including it without a reason (like having a combined Autonomous and TeleOp phased file), I'll know they didn't take the time to read this important readme file.


JacobJoystickDriver.h

This header serves as a library that overlays the JoystickDriver.h file that everyone uses. Many teams have edited the main driver to add extra functionality, but this isn't necessarily good coding practice. What we can do instead is create a header that imports the JoystickDriver.h, add new bits of code to this file, and then include this new file in our main programs. I don't say that this header file "replaces" JoystickDriver.h because you still need to have that file on your computer, BUT you don't include it anymore.


Tetrix.h and NXT.h

This is a suite of a couple movement commands made primarily for Autonomous mode. It contains pMotor, the thing that keeps motors from burning out, and uses inches and degrees to drive the robot instead of encoder counts. There are two versions of this header, one for the Tetrix platform and another for the NXT platform (using NXT servos as drive motors). In the Tetrix version, you'll have to pass the names of your motors since every robot has a different configuration. The NXT version uses the B-Left C-Right standard and it's easier to swap the wires than re-write the program.


SarahTankDrive.h

My friend Sarah from FTC 5096 Monkey Madness contributed this file. To be honest, I haven't really looked through it yet, but Sarah made it so I'm sure it's great. I can tell you that it's for the TeleOp period and since most of what I'm doing is for the Autonomous phase, I consider it a good addition.


MenuSystem.h

This header adds menu functionality to a program. It can be a memory hog and is still in early development, but as of now, it's stable and with coupled with the AutonomousMenu.h header, it workes great for an Autonomous "Program Chooser."


AutonomousMenu.h

This header goes inbetween the Menu system and your autonomous program. It contains the Delayed start function and is a better platform to add to than MenuSystem.h if you want to do some tweaking to your menu.


NXT Text File IO.h

I actually stole this file. I didn't write it at all. There is an example file called "NXT Text File IO.c" and I essentially trimmed the fat (the task main()) so I could use the functions inside it. It's only used in the Calibration tool, but it's actually pretty handy for data logging and you'll probably see me using it inside Tetrix.h or NXT.h soon for that purpose. Actually, I think I'll create a logging module... Yeah, I like that idea better. It might be very beneficial to see exactly what the robot was thinking during that awful Autonomous round.




	Examples


MenuSystemTest.c

This is currently a working example for MenuSystem and AutonomousMenu. (Later I hope to develop it into an advanced autonomous template.


NXT Motion Test.c

This is a working example for NXT.h which showcases its ease of use and accuracy.





	The Calibration Tool

The calibration tool for pMotor is a complex work of art. First off, it's bilingual: it collects data with the RobotC program NXT_Calibrate.c and analyzes it with a Python program CalibrationDataParser.py.  In order to use the Python part of the Calibration Tool, you'll need to have matplotlib and numpy installed. (Possibly scipy, too, and it's not difficult to add once you have matplotlib and numpy installed. The hard part was getting the right python configuration. I'm using Python 3.2.5 32-bit on a Windows7 64-bit OS. And when you install Python, you have to tell it you just want to install it "for me only" not "everyone using this computer." Whatever.)  The python half can create really great graphs and stuff to basically assist me in interpolating Velocity-Deceleration scatter plots. With all the data this collects, I should be able to figure out a trend and program a function into RobotC that will circumvent the Python part.

tl;dr: This thing isn't done yet. It only draws fancy pictures and makes the robot go back and forth.






