#pragma systemFile
#pragma autoStartTasks

#warning "Thanks for choosing UniversalLib!"

/*
This merged edition of UniversalLib consists of a small addition to the joystick driver and
a whole bunch of movement code.

This version of the Library is using Tetrix.h, since that is the most common type, and
it expects at least 4 wheels on the drivetrain.

TODO:
  1. Make this code work for 2, 4, or 6 drive motors.

*/
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//
//                      JacobJoystickDriver.h
//
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//
//                     		Button Toggling
//
////////////////////////////////////////////////////////////////////////

#include "JoystickDriver.c"

// Initialize all the boolean arrays necessary for button toggling
bool joy1BtnBool[12], joy2BtnBool[12], joy1BtnPrevState[12], joy2BtnPrevState[12];

// Transform those variables into arrays with nothing but false boolean values
task initvariables(){
	memset(joy1BtnBool, false, sizeof(joy1BtnBool));
	memset(joy2BtnBool, false, sizeof(joy2BtnBool));
	memset(joy1BtnPrevState, false, sizeof(joy1BtnPrevState));
	memset(joy2BtnPrevState, false, sizeof(joy2BtnPrevState));
}

// We call the function joy#Btn(), passing the value of button to that function, returning a boolean for the button press.
// The index is always the number of the button minus one because there is no zero button but there is a zero index.

short joy1Tog(int btn)
{
	// if the button is being pressed
	if(joy1Btn(btn))
	{
		//if the past value of the array is true, this is the first iteration in which the button is pressed.
		if(joy1BtnPrevState[btn-1])
		{
			//change the array to false so we don't run this loop more than once
			joy1BtnPrevState[btn-1] = false;
			joy1BtnBool[btn-1] = !joy1BtnBool[btn-1];
		}
	}else{
		joy1BtnPrevState[btn-1] = true;
	}
	return joy1BtnBool[btn-1]; // return the state of the toggle
}

short joy2Tog(int btn)
{
	// if the button is being pressed
	if(joy2Btn(btn))
	{
		//if the past value of the array is true, this is the first iteration in which the button is pressed.
		if(joy2BtnPrevState[btn-1])
		{
			//change the array to false so we don't run this loop more than once
			joy2BtnPrevState[btn-1] = false;
			joy2BtnBool[btn-1] = !joy2BtnBool[btn-1];
		}
	}else{
		joy2BtnPrevState[btn-1] = true;
	}
	return joy2BtnBool[btn-1]; //return the state of the toggle.
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//
//                             Tetrix.h
//
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// Used in linear and angular motion
#define WHEEL_DIAMETER 3 //in whatever unit (Inches, cm) as long as it matches the unit you input
#define GEAR_RATIO (3/2) // Driver / driven [https://www.youtube.com/watch?v=D_i3PJIYtuY]
#define ENC_COUNTS_PER_REVOLUTION (1440 / GEAR_RATIO) // Do not change
#define INCH_CONVERSION (ENC_COUNTS_PER_REVOLUTION/(PI*WHEEL_DIAMETER))

// Used only in angular motion
#define WHEEL_BASE 11.75 //width of the wheel base in the same unit used above.
#define DEGREE_CONVERSION ((WHEEL_BASE*ENC_COUNTS_PER_REVOLUTION)/(360*WHEEL_DIAMETER))
//The actual equation is WHEEL_BASE*PI*ENC_COUNTS_PER_REVOLUTION / 360dg*WHEEL_DIAMETER*PI,
//but the PIs cancel out, so this equation is dramatically simplified.

// Used in gyroscopic turns
#define GYROSCOPE_OFFSET 0

////////////////////////////////////////////////////////////////////////
//
//	               Global Motor and Sensor Names
//
////////////////////////////////////////////////////////////////////////
// This creates global variables for the motors that we can reference.
// syntax: global_motors(Left Motor with the Encoder, Right motor with the Encoder, Left motor without encoder, Right motor without encoder);
//
// This can also create a variable for the gyroscope.
// syntax: get_gyroscope_sensor(Gyro sensor name);
// Put these before the waitForStart();

tMotor LeftEnc;
tMotor RightEnc;
tMotor LeftAux;
tMotor RightAux;

void global_motors(tMotor LETemp, tMotor RETemp, tMotor LATemp, tMotor RATemp)
{
	LeftEnc = LETemp;
	RightEnc = RETemp;
	LeftAux = LATemp;
	RightAux = RATemp;
}

tSensors Gyroscope;
bool IS_GYRO_SENSOR = false;

void get_gyroscope_sensor(tSensors gyroTemp)
{
	Gyroscope = gyroTemp;
	IS_GYRO_SENSOR = true;
}

////////////////////////////////////////////////////////////////////////
//
//                   		Halting and Debugging
//
////////////////////////////////////////////////////////////////////////

//optional debugging function. You can drop a "debug_stop();" at any point in the code
//to have it pause processing until the orange button on the NXT is pressed
void debug_stop()
{
	while(nNxtButtonPressed!=3)
	{}
	return;
}

//function used to stop the drive train for the next movement, pausing for one second.
void halt_drive()
{
	motor[LeftEnc]= 0; motor[LeftAux]= 0; motor[RightEnc]= 0; motor[RightAux]= 0;
	wait1Msec(800);
	return;
}

////////////////////////////////////////////////////////////////////////
//
//												Motion Controls
//
////////////////////////////////////////////////////////////////////////

//
//	pMotor Core System
//

long GYRO_TIME = 0; //Used to Integrate the Gyroscope
long GYRO_ANGULAR_DISPLACEMENT = 0;

bool targeting_system(int TrackingMethod, int Target)
/*
This system is responsible for returning a boolean for whether a movement has reached its target.
*/
{
	switch(TrackingMethod)
	{
	case 1://Encoder based movement
		return ((abs(nMotorEncoder[LeftEnc]) < Target) || (abs(nMotorEncoder[RightEnc]) < Target));

	case 2://Gyroscopic based movement	Works in theory
		long angularVelocity = SensorValue(Gyroscope) - GYROSCOPE_OFFSET;
		float Gyro_ChangeInTime = time1[T2] - GYRO_TIME;
		GYRO_ANGULAR_DISPLACEMENT += angularVelocity * Gyro_ChangeInTime;
		return abs(GYRO_ANGULAR_DISPLACEMENT) < Target; //Gyroscope has yet to reach target should return True
	}
	return false; //This will never execute, but the compiler looks for it regardless
}

bool pMotor(int TrackingMethod, int Target, int LeftSpeed, int RightSpeed)
{
	bool stopMotors = false;
	int changeInCounts = 0;
	int previousCounts = -100; //This is an offset for the first check since the robot is
														 //just still accelerating.
	int threshhold = ((abs(LeftSpeed) + abs(RightSpeed))/2) * 5; //Doesn't matter which speed we use, but we'll average them just in case we want to direct access this function
	int previousTime = time100[T2];														 // abs() of each speed is taken because they can sometimes be in opposite directions
	GYRO_TIME = time1[T2];
	GYRO_ANGULAR_DISPLACEMENT = 0;

	nMotorEncoder[LeftEnc]= 0; nMotorEncoder[RightEnc]= 0;

	while( targeting_system(TrackingMethod, Target) && (!stopMotors))
	{
		motor[LeftEnc]= LeftSpeed; motor[LeftAux]= LeftSpeed; motor[RightEnc]= RightSpeed; motor[RightAux]= RightSpeed;

		if(time100[T2] > 6+previousTime)
		{
												//get the average distance traveled on both wheels
			changeInCounts = ((abs(nMotorEncoder[LeftEnc]) + abs(nMotorEncoder[RightEnc])) / 2) - previousCounts;
			previousCounts = (abs(nMotorEncoder[LeftEnc]) + abs(nMotorEncoder[RightEnc])) / 2;

			if (changeInCounts < threshhold)
			{
				stopMotors = true;
				PlayTone(440, 30);
			}
			writeDebugStreamLine("nMotorEncoder[LeftEnc] = %d  nMotorEncoder[RightEnc] = %d", nMotorEncoder[LeftEnc], nMotorEncoder[RightEnc]);
			writeDebugStreamLine("changeInCounts = %d \t threshhold = %d", changeInCounts, threshhold);
			previousTime = time100[T2];
		}
	}
	halt_drive();
	return (!stopMotors); // This is the success value. If stopMotors is true, then it didn't succeed and vice versa.
}

//
//	Linear motion
//

bool linear(int inchTarget, int speed, bool deltaCorrection = false)
{
	int LeftSpeed = speed * sgn(inchTarget);
	int RightSpeed = LeftSpeed;
	int encoderTarget = abs(inchTarget * INCH_CONVERSION); //convert from inches to encoder counts and take the absolute value so we just have a distance
	writeDebugStreamLine("\nlinear movement: %d inches  %d Encoder Target  %d LeftSpeed  %d RightSpeed", inchTarget, encoderTarget, LeftSpeed, RightSpeed);

	bool movementSuccess = pMotor(1, encoderTarget, LeftSpeed, RightSpeed);

	if(movementSuccess && deltaCorrection)
	{
		int drift = (((abs(nMotorEncoder[LeftEnc]) + abs(nMotorEncoder[RightEnc])) / 2) - abs(encoderTarget)); //initial - final returns a negative number
		int driftLeft = LeftSpeed/-5;
		int driftRight = RightSpeed/-5;
		movementSuccess = (movementSuccess && pMotor(1, drift, driftLeft, driftRight) );
		writeDebugStreamLine("correction: Encoder counts reversed: %d  driftLeft: %d  driftRight %d", drift, driftLeft, driftRight);
	}

	if(movementSuccess) //Just a debug write
	{
		writeDebugStreamLine("movementSuccess: SUCCESS");
	}else{
		writeDebugStreamLine("movementSuccess: FAILURE");
	}

	return movementSuccess;
}

//
//	Angular motion
//

bool angular(int degrees, int speed, bool deltaCorrection = false)
/*
angular checks if there is a gyro sensor in use. If there is, it defaults to using it to calculate movement. If not, it calculates movements using encoders.
*/
{
	int RightSpeed = speed * sgn(degrees);
	int LeftSpeed = -RightSpeed;
	int Target;
	bool movementSuccess;

	if(IS_GYRO_SENSOR)
	{
		Target = degrees;
		writeDebugStreamLine("angular movement *GYRO*: %d degrees  %d LeftSpeed  %d RightSpeed", degrees, LeftSpeed, RightSpeed);
		movementSuccess = pMotor(2, Target, LeftSpeed, RightSpeed);
  }else{
		Target = abs(degrees * DEGREE_CONVERSION); //convert from degrees to encoder counts
		writeDebugStreamLine("angular movement: %d degrees  %d Encoder Target  %d LeftSpeed  %d RightSpeed", degrees, Target, LeftSpeed, RightSpeed);
		movementSuccess = pMotor(1, Target, LeftSpeed, RightSpeed);
	}

	if(movementSuccess && deltaCorrection)
	{
		int drift = (((abs(nMotorEncoder[LeftEnc]) + abs(nMotorEncoder[RightEnc])) / 2) - abs(degrees * DEGREE_CONVERSION)); //initial - final returns a negative number
		int driftLeft = LeftSpeed/-5;
		int driftRight = RightSpeed/-5;
		movementSuccess = (movementSuccess && pMotor(1, drift, driftLeft, driftRight) );
		writeDebugStreamLine("correction: Encoder counts reversed: %d  driftLeft: %d  driftRight %d", drift, driftLeft, driftRight);
	}

	if(movementSuccess) //Just a debug write
	{
		writeDebugStreamLine("movementSuccess: SUCCESS");
	}else{
		writeDebugStreamLine("movementSuccess: FAILURE");
	}

	return movementSuccess;
}
